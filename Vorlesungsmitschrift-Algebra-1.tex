\documentclass[11pt,a4paper, oneside, openany]{book}

\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{wasysym}
\usepackage{enumitem}
\usepackage{titlesec}
\usepackage{amsthm}


%präambel
%\titleformat{\chapter}[hang]{\Huge\bfseries}{\thechapter}{0pt}{\Huge\bfseries}
\titleformat{\chapter}[hang]{\Huge\bfseries}{Kapitel \thechapter:\:}{0pt}{}{}
\title{\huge Vorlesungsmitschrift Algebra 1\\WS 2019/20\\Prof. Dr. Hien}

\newtheorem{defi}{Definition}[chapter]
\newtheorem{satz}[defi]{Satz}
\newtheorem{lemma}[defi]{Lemma}
\newtheorem{korollar}[defi]{Korollar}
\newtheorem{bem}[defi]{Bemerkung}
%endpräambel


\begin{document}
\maketitle
\tableofcontents


\chapter{Motivation und Teaser}

Die quadratische Gleichung $aX^2+bX+c=0$ mit $a,b,c\in \mathbb{K}$ (wobei $\mathbb{K}$ ein beliebiger Körper außer $\mathbb{F}_2=\mathbb{Z}/2\mathbb{Z}$ ist) und $a\not=0$ lässt sich mit der folgenden so bekannten Mitternachtsformel lösen: 
\begin{equation*}
x_{1/2} = \frac{-b\pm\sqrt{b^2 - 4\cdot a\cdot c}}{2\cdot a}
\end{equation*}
\\Wie geht die Mitternachtsformel für Gleichungen von Grad $\geq 3$ weiter?
\\\underline{Grad 3}: Sei $\not=a$
\begin{equation*}
f(X):=aX^3+bX^2+cX+d=0
\end{equation*}
\\O.B.d.A.: $a=1$
\\Betrachte: $g(X):=f(X-\frac{b}{3})=...=X^3+pX+q$
\\Dann lautet die Formel von Cardano/Tartaglia (16.Jhdt.):
\begin{equation*}
\sqrt[3]{-\frac{q}{2} + \sqrt{\left(\frac{q}{2}\right)^2 + \left(\frac{p}{3}\right)^3}} + \sqrt[3]{-\frac{q}{2} - \sqrt{\left(\frac{q}{2}\right)^2 + \left(\frac{p}{3}\right)^3}}
\end{equation*}
\\ \underline{Grad 4:} Auch hier existiert eine Formel von Cardano \& Gefährten
\\\\ \underline{Grad $\geq 5$:} Es gibt keine Formel. (Theorem von Galois, Abel)
\\
\\ Schlimmer noch: die Gleichung $X^5-4X+2=0$ hat keine Lösung, die man mittels +, $\cdot, \sqrt[k]{ }$ schreiben kann. (Natürlich hat die Gleichung 5 Lösungen in $\mathbb{C}$.)



\chapter{Gruppen}

\begin{defi} \label{Gruppe}
Eine \textbf{Gruppe} ist eine Menge G zusammen mit einer Abbildung $\cdot:G \times G \to G,\, (g,h) \mapsto g \cdot h$ und einem linksneutralem Element $e \in G$ mit
\begin{enumerate}[label=(\roman*)]
\item $\forall g \in G: e\cdot g=g$
\item $\forall g,h,k \in G: (g \cdot h) \cdot k = g \cdot (h \cdot k)$ (Assoziativität)
\item $\forall g \in G \exists h \in G: h \cdot g = e$ (linksinverses Element)
\end{enumerate}
\end{defi}

\begin{lemma}
Ist $(G,\cdot,e$) eine Gruppe, so gilt:
\begin{enumerate}[label=(\roman*)]
\item e ist auch rechtsneutral d.h. $\forall g\in G: g\cdot e=g$
\item Das zu $g\in G$ linksinverse Element ist auch rechtsinvers, d.h. $g\cdot h=e$, und eindeutig durch $g$ festgelegt
\end{enumerate}
\end{lemma}

\begin{proof}\leavevmode
\begin{enumerate}[label=(\roman*):]
\item Für $g\in G$ und Linksinverses $h\in G$ gilt $h\cdot g=e$. Auch $h\in G$ hat nach Definition \ref{Gruppe} (iii) ein Linksinverses, sagen wir $k\in G $ mit $ k\cdot h=e$. Dann ist 
\begin{equation*}
g \cdot h  
\overset{\tiny{(i)}}{=}  
e \cdot (g \cdot h)  
\underset{\tiny{(iii)}}{\overset{\tiny{Vor}}{=}}
(k \cdot h) \cdot (g \cdot h)  
\overset{\tiny{(ii)}}{=} 
k \cdot (h \cdot g) \cdot h  
\overset{\tiny{Vor.}}{=}  
k \cdot e \cdot h  
\overset{\tiny{(i)}}{=}  
k \cdot h  
\overset{\tiny{Vor.}}{=}  
e
\end{equation*}
\item Ist auch $\overset{\tiny{\sim}}{e} \in G$ linksneutral (somit nach oben auch rechtsneutral) dann gilt wegen Rechtsneutralität von $\overset{\tiny{\sim}}{e}$: 
\begin{equation*}
e  =  e \cdot \overset{\tiny{\sim}}{e}  =  \overset{\tiny{\sim}}{e}
\end{equation*}
Ist auch $\overset{\tiny{\sim}}{h} \in G$ linksinvers zu $g$ (also auch nach oben rechtsinvers), so gilt (ähnlich wie oben):
\begin{equation*}
\overset{\tiny{\sim}}{h}  =  
\overset{\tiny{\sim}}{h} \cdot e  =
\overset{\tiny{\sim}}{h} \cdot (g \cdot h)  =
(\overset{\tiny{\sim}}{h} \cdot g) \cdot h  =
e \cdot h =
h
\end{equation*} 
\end{enumerate}
\end{proof}

\begin{bem}
Wir schreiben $g^{-1} \in G$ für \underline{das} eindeutig bestimmte Inverse zu $g \in G$
\end{bem}

\begin{lemma}
In einer Gruppe $(G, \cdot, e)$ gilt:
\begin{itemize}
\item $\forall g \in G: (g^{-1})^{-1} = g$
\item $\forall g,h \in G: (g \cdot h)^{-1} = h^{-1} \cdot g^{-1}$
\end{itemize}
\end{lemma}
\begin{proof}
zum ersten Punkt: Es ist zu zeigen, dass $g$ das Linksinverse zu $g^{-1}$.
Es gilt wegen obigem Lemma, dass $g^{-1}$ auch rechtsinvers ist und somit folgt:
\begin{equation*}
g \cdot (g^{-1}) = e
\end{equation*}
Zum zweiten Punkt: siehe Übungsaufgabe 3 Blatt 1
\end{proof}

\begin{defi}
Eine Gruppe $(G, \cdot, e)$ heißt \textbf{abelsch}, falls gilt: 
\begin{equation*}
\forall g,h \in G: g \cdot h = h \cdot g
\end{equation*}
\end{defi}

\begin{bem}
Ist $(G, \cdot , e)$ abelsch, so schreibt man oft + statt $\cdot$.
\\Insbesondere folgt:
\begin{center}
\begin{tabular}{cc}
allgemeine Gruppe  &  abelsche Gruppe
\\$g \cdot h$  &  $g+h$
\\$g^{-1}$  &  $-g$
\\$g^m := 
\begin{cases}
\overset{|m|\text{-Mal}}{\overbrace{g \cdot ... \cdot g}}  &  m>0
\\g^{-1} \cdot ... \cdot g^{-1}  &  m<0
\\e  & m=0
\end{cases}$
& $m \cdot g$
\end{tabular}
\end{center}

\end{bem}
\end{document}
